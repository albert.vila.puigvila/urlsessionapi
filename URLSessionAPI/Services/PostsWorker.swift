//
//  PostsWorker.swift
//  URLSessionClient
//
//  Created by albert vila  on 03/03/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

typealias ResultHandler<T: Decodable, E: Error> = (Result<T, E>) -> Void

class WorkerPosts {
    let service: ServiceProtocol
    let provider: URLSessionProvider
    
    init(service: ServiceProtocol, sessionProvider: URLSessionProvider = URLSessionProvider()) {
        self.service = service
        self.provider = sessionProvider
    }
    
    func performRequest<T: Decodable>(type: T.Type, completion: @escaping ResultHandler<T, NetworkError>) {
        provider.request(type: type, service: service, completion: completion)
    }
}
