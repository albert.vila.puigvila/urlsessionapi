//
//  PostServices.swift
//  URLSessionClient
//
//  Created by albert vila  on 20/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

enum Services: ServiceProtocol {
    
    /// List of services
    case allPosts
    case postsOfUser(_ id: String)
    /*
     /// add services
     */

    /// ServiceProtocol
    var baseURL: URL {
        return URL(string: "https://jsonplaceholder.typicode.com")!
    }
    
    var path: String {
        switch self {
        case .allPosts:
            return "/posts"
        case .postsOfUser:
            return "/posts"
        }
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var task: Task {
        switch self {
        case .allPosts:
            return .requestPlain
        case .postsOfUser(let id):
            let params = ["userId": id]
            return .requestParameters(params)
        }
    }
    
    var headers: Headers? {
        let auth = Headers.Value(key: .authorization, value: "Bearer 1234")
        let contentType = Headers.Value(key: .contentType, value: "application/json")
        do {
            return try Headers(headers: [auth, contentType])
        } catch {
            return nil
        }
    }
    
    var parametersEncoding: ParametersEncoding {
        return .url
    }
}
