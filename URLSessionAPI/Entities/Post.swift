//
//  Post.swift
//  URLSessionClient
//
//  Created by albert vila  on 20/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

struct Post: Decodable {
    let userId: Int
    let title: String
    let id: Int
    let body: String
    
    enum CodingKeys: String, CodingKey {
        case userId
        case title
        case id
        case body
    }
    
    init(from decoder: Decoder) throws {
        let values  = try decoder.container(keyedBy: CodingKeys.self)
        
        self.userId = try values.decode(Int.self, forKey: .userId)
        self.title  = try values.decode(String.self, forKey: .title)
        self.id     = try values.decode(Int.self, forKey: .id)
        self.body   = try values.decode(String.self, forKey: .body)
    }
}
