//
//  NetworkErrors.swift
//  URLSessionClient
//
//  Created by albert vila  on 18/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

enum NetworkError: Error, Equatable {
    case unknown
    case authorizationHeader
    case bearerHeader(String)
    case duplicatedHeader(String)
    case noResponse
    case noData
}

extension NetworkError: LocalizedError {
    var localizedDescription: String {
        switch self {
        case .unknown: return ""
        case .noData: return "No data in your response"
        case .noResponse: return "⚠️ No data response"
        case .authorizationHeader: return "⚠️ Authorization header is missing"
        case .bearerHeader(let message): return "⚠️ Bearer string is \(message)"
        case .duplicatedHeader(let message): return "⚠️ \(message) header is duplicated"
        }
    }
}
