//
//  ProviderProtocol.swift
//  URLSessionClient
//
//  Created by albert vila  on 19/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

protocol ProviderProtocol {
    
    func request<T>(type: T.Type,
                    service: ServiceProtocol,
                    completion: @escaping (Result<T, NetworkError>) -> ()) where T: Decodable
}

// MARK: - URLSessionProvider -
final class URLSessionProvider: ProviderProtocol {
    private let successRange: CountableClosedRange<Int> = 200...299
    
    let session: URLSessionProtocol

    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func request<T>(type: T.Type,
                    service: ServiceProtocol,
                    completion: @escaping (Result<T, NetworkError>) -> ()) where T : Decodable {
        do {
            let request = try URLRequest(service: service)
            let task = session.dataTask(request: request, completionHandler: { [weak self] data, response, error in
                let httpResponse = response as? HTTPURLResponse
                self?.handleDataResponse(data: data, response: httpResponse, error: error, completion: completion)
            })
            task.resume()
        } catch {
            assertionFailure(error.localizedDescription)
        }
    }
    
    private func handleDataResponse<T: Decodable>(data: Data?,
                                                  response: HTTPURLResponse?,
                                                  error: Error?,
                                                  completion: (Result<T, NetworkError>) -> Void) {
        guard error == nil else {
            return completion(.failure(.unknown))
        }
        guard let response = response else {
            return completion(.failure(.noResponse))
        }
        guard successRange.contains(response.statusCode) else {
            return completion(.failure(.unknown))
        }
        guard let data = data else {
            return completion(.failure(.noResponse))
        }
        
        do {
            let model = try JSONDecoder().decode(T.self, from: data)
            completion(.success(model))
        } catch {
            assertionFailure(error.localizedDescription)
            completion(.failure(.unknown))
        }
    }
}
