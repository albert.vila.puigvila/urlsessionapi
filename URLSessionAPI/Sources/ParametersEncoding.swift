//
//  ParametersEncoding.swift
//  URLSessionClient
//
//  Created by albert vila  on 18/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

enum ParametersEncoding {
    case url
    case body
}
