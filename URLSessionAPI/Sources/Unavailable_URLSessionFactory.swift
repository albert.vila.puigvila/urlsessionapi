//
//  URLSessionFactory.swift
//  URLSessionClient
//
//  Created by albert vila  on 19/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

// MARK: - Maybe for V2 -


/*
protocol URLSessionFactoryProtocol {
    var maxConcurrentOperations: Int { get }
    var option: URLSessionFactoryOption { get }
}

extension URLSessionFactoryProtocol {
    var session: URLSessionProtocol {
        return URLSessionFactory(values: self).session
    }
}

// MARK: - Helper to init workers, interactor's with `URLSessionProtocol` -
final class URLSessionFactory {
    
    lazy var session: URLSessionProtocol = {
        let element = URLSession(configuration: values.option.rawValue)
        element.delegateQueue.maxConcurrentOperationCount = values.maxConcurrentOperations
        return element
    }()
    
    let values: URLSessionFactoryProtocol
    
    /// Delegated Init
    ///
    /// - Parameter configuration: instantiate Configuration object, by default .ephemeral
    init(values: URLSessionFactoryProtocol) {
        self.values = values
     }
}

enum URLSessionFactoryOption: RawRepresentable {
    typealias RawValue = URLSessionConfiguration
    
    case `default`
    case ephemeral
    case background(String)
    
    var rawValue: URLSessionConfiguration {
        switch self {
        case .default:
            return .default
        case .ephemeral:
            return .ephemeral
        case .background(let identifier):
            return .background(withIdentifier: identifier)
        }
    }
    
    init?(rawValue: RawValue) {
        fatalError()
    }
}
*/
