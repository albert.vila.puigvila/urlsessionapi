//
//  ServiceProtocol.swift
//  URLSessionClient
//
//  Created by albert vila  on 18/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

protocol ServiceProtocol {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var task: Task { get }
    var headers: Headers? { get }
    var parametersEncoding: ParametersEncoding { get }
//    var evaluateAuthToken: Bool { get }
}
