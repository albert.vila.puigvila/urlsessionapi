//
//  Headers.swift
//  URLSessionClient
//
//  Created by albert vila  on 18/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

//typealias Header = [HeaderKey: String]

struct Headers {
    struct Value: Equatable {
        let key: HeaderKey
        let value: String
    }
    typealias Elements = [Value]
    
    private(set) var headers = Elements()
    
    // MARK: - Life Cycle -
    init() { }
    
    init(headers: Elements) throws {
        try headers.forEach { try insert(header: $0) }
    }
}

// MARK: - mutating func's -
extension Headers {
    mutating func insert(header: Value) throws {
        guard headers.first(where: { $0.key == header.key }) == nil else {
            throw NetworkError.duplicatedHeader(header.key.rawValue)
        }
        headers.append(header)
        
        try evaluateAuthorizationHeader()
    }
    
    mutating func clear() {
        headers.removeAll()
    }
}

// MARK: - public -
extension Headers {

    private func evaluateAuthorizationHeader() throws {
        guard let auth = headers.first(where: { $0.key == .authorization }) else {
            return
        }
        guard let component = auth.value.components(separatedBy: " ").first, (component == "Bearer" || component == "bearer") else {
            throw NetworkError.bearerHeader("missing")
        }
        if case component = "bearer" {
            throw NetworkError.bearerHeader("lowercase")
        }
    }
}

// MARK: - Collection protocol -
extension Headers: Collection {
    typealias Index = Elements.Index
    typealias Element = Elements.Element
    
    var startIndex: Index {
        return headers.startIndex
    }
    var endIndex: Index {
        return headers.endIndex
    }
    
    func index(after index: Index) -> Index {
        return headers.index(after: index)
    }
    
    subscript(index: Index) -> Element {
        return headers[index]
    }
}

enum HeaderKey: String {
    case authorization = "Authorization"
    case contentType   = "Content-Type"
}
