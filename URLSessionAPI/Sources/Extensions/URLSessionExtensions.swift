//
//  URLSessionExtensions.swift
//  URLSessionAPI
//
//  Created by albert vila on 04/03/2020.
//  Copyright © 2020 albert vila. All rights reserved.
//

import Foundation
    
typealias DataTaskResult = (Data?, URLResponse?, Error?) -> ()

// MARK: - URLSessionProtocol -

/// Protocol helper for URLSession
protocol URLSessionProtocol {
    func dataTask(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol
}

extension URLSession: URLSessionProtocol {
    func dataTask(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        return dataTask(with: request, completionHandler: completionHandler)
    }
}

// MARK: - URLSessionDataTaskProtocol -

/// Protocol helper for URLSessionDataTask
protocol URLSessionDataTaskProtocol {
    func resume()
    func cancel()
}
extension URLSessionDataTask: URLSessionDataTaskProtocol { }
