//
//  Dictionary.swift
//  URLSessionClient
//
//  Created by albert vila  on 19/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

extension Dictionary {
    func contains(key: Key) -> String? {
        let value = first { $0.key == key }
        guard let dic = value else {
            return nil
        }
        guard let val = dic.value as? String else {
            return nil
        }
        return val
    }
}
