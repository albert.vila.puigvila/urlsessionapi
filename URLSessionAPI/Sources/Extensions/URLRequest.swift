//
//  URLRequest.swift
//  URLSessionClient
//
//  Created by albert vila  on 18/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

extension URLRequest {

    init(service: ServiceProtocol) throws {
        guard let urlComponents = URLComponents(service: service) else {
            throw URLRequestError.invalidURLComponents
        }
        guard let url = urlComponents.url else {
            throw URLRequestError.invalidURL
        }
        self.init(url: url)
        
        httpMethod = service.method.rawValue
        
        service.headers?.forEach { header in
            addValue(header.value, forHTTPHeaderField: header.key.rawValue)
        }
        guard case let .requestParameters(parameters) = service.task,
            service.parametersEncoding == .body else {
                return
        }
        httpBody = try JSONSerialization.data(withJSONObject: parameters)
    }
}

enum URLRequestError: Error, LocalizedError {
    case invalidURLComponents
    case invalidURL
    
    var localizedDescription: String {
        switch self {
        case .invalidURLComponents: return "Impossible to create a URLComponents"
        case .invalidURL: return "Impossible to create a URL"
        }
    }
}
