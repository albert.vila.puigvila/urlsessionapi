//
//  URLSessionClientTests.swift
//  URLSessionClientTests
//
//  Created by albert vila  on 20/02/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import XCTest
@testable
import URLSessionAPI

class URLComponentsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testUrlParamsEncoding() {
        let componentsAllPosts = URLComponents(service: ServiceSpy(encoding: .url, service: .allPosts))!
        XCTAssertEqual(componentsAllPosts.url?.absoluteString, "http://alf.com/posts")
        
        let components = URLComponents(service: ServiceSpy(encoding: .url, service: .userPost("1")))!
        XCTAssertEqual(components.url?.absoluteString, "http://alf.com/posts?userId=1")
    }
    
    func testJSONParamsEncoding() {
        let componentsAllPosts = URLComponents(service: ServiceSpy(encoding: .body, service: .allPosts))!
        XCTAssertEqual(componentsAllPosts.url?.absoluteString, "http://alf.com/posts")
        
        let components = URLComponents(service: ServiceSpy(encoding: .body, service: .userPost("1")))!
        XCTAssertEqual(components.url?.absoluteString, "http://alf.com/posts")
    }
    
    func testURLRequest() {
        
    }
}

private extension URLComponentsTests {
    struct ServiceSpy: ServiceProtocol {        
        enum Service {
            case allPosts
            case userPost(String)
        }
        
        // ServiceProtocol
        var baseURL: URL {
            return URL(string: "http://alf.com/")!
        }
        var path: String {
            switch service {
            case .allPosts: return "posts"
            case .userPost: return "posts"
            }

        }
        var method: HTTPMethod {
            return .get
        }
        var task: Task {
            switch service {
            case .allPosts:
                return .requestPlain
            case let .userPost(userId):
                return .requestParameters(["userId": userId])
            }
        }
        var headers: Headers? {
            return nil
        }
        var parametersEncoding: ParametersEncoding
        
        // class var's
        var service: Service
        
        // MARK: - Life Cycle -
        init(encoding: ParametersEncoding, service: Service) {
            self.parametersEncoding = encoding
            self.service            = service
        }
    }
}
