//
//  WorkersPostsTests.swift
//  URLSessionClientTests
//
//  Created by albert vila  on 04/03/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import XCTest
@testable
import URLSessionAPI

class WorkersPostsTests: XCTestCase {
    
    func testSuccess() {
        let json = """
        [{"userId": 1, "id": 1, "title": "hi", "body": "bla"},
         {"userId": 1, "id": 2, "title": "hello", "body": "bla bla"}]
        """
        let mockSession = MockURLSession(response: .success(json: json), delayMillis: 250)
        
        let provider = URLSessionProvider(session: mockSession)
        let sut = WorkerPosts(service: Services.allPosts, sessionProvider: provider)
        
        let exp = expectation(description: "\(#function)")
        
        var resultData = [Post]()
        sut.performRequest(type: [Post].self) { result in
            guard let data = try? result.get() else {
                return XCTFail()
            }
            resultData = data
            exp.fulfill()
        }
        wait(for: [exp], timeout: 0.750)
        XCTAssertEqual(resultData.count, 2)
        
        let requestedURLs = (sut.provider.session as! MockURLSession).finishedTasks.map { return $0.urlRequest.url!.absoluteString }
        let expectedURLs = ["https://jsonplaceholder.typicode.com/posts"]
        XCTAssertEqual(requestedURLs, expectedURLs)
        XCTAssert((sut.provider.session as! MockURLSession).executingTasks.count == 0)
    }

    func testFailureStatusCode() {
        let json = """
        [{"userId": 1, "id": 1, "title": "hi", "body": "bla"}]
        """
        let mockSession = MockURLSession(response: .success(json: json),
                                         urlResponse: HTTPURLResponse(statusCode: 500),
                                         delayMillis: 200)
        
        let provider = URLSessionProvider(session: mockSession)
        let sut = WorkerPosts(service: Services.postsOfUser("1"), sessionProvider: provider)
        
        let exp = expectation(description: "\(#function)")
        
        var resultData = [Post]()
        var error: NetworkError!
        sut.performRequest(type: [Post].self) { result in
            switch result {
            case .success(let posts):
                resultData = posts
            case .failure(let aError):
                error = aError
            }
            exp.fulfill()
        }
        XCTAssert((sut.provider.session as! MockURLSession).executingTasks.count == 1)
        wait(for: [exp], timeout: 0.750)
        
        XCTAssert(resultData.isEmpty)
        XCTAssertEqual(error, NetworkError.unknown)
        
        let requestedURLs = (sut.provider.session as! MockURLSession).finishedTasks.map { return $0.urlRequest.url!.absoluteString }
        let expectedURLs = ["https://jsonplaceholder.typicode.com/posts?userId=1"]
        XCTAssertEqual(requestedURLs, expectedURLs)
        XCTAssert((sut.provider.session as! MockURLSession).executingTasks.count == 0)
    }
}
