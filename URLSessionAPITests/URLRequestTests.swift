//
//  URLRequestTests.swift
//  URLSessionAPITests
//
//  Created by albert vila on 04/03/2020.
//  Copyright © 2020 albert vila. All rights reserved.
//

import XCTest
@testable
import URLSessionAPI

class URLRequestTests: XCTestCase {
    
    func testGetMethod() {
        let service = ServiceProtocolSpy(path: "posts",
                                         method: .get,
                                         task: .requestPlain,
                                         headers: nil,
                                         parametersEncoding: .url)
        do {
            let url = try URLRequest(service: service)
            XCTAssertEqual(url.httpMethod!, HTTPMethod.get.rawValue)
            XCTAssertEqual(url.url?.absoluteString, "http://mango.com/posts")
            XCTAssert(url.httpBody == nil)
            XCTAssertEqual(url.allHTTPHeaderFields?.keys.count, 0)
        } catch {
            XCTFail()
        }
    }
    
    func testCorrectHeaders() throws {
        let token = "Bearer 1234"
        let cType = "application/json"
        let auth = Headers.Value(key: .authorization, value: token)
        let contentType = Headers.Value(key: .contentType, value: cType)
        
        let headers = try Headers(headers: [auth, contentType])
        let service = ServiceProtocolSpy(path: "posts",
                                         method: .get,
                                         task: .requestPlain,
                                         headers: headers,
                                         parametersEncoding: .url)
        do {
            let url = try URLRequest(service: service)
            XCTAssertEqual(url.httpMethod!, HTTPMethod.get.rawValue)
            XCTAssertEqual(url.url?.absoluteString, "http://mango.com/posts")
            XCTAssert(url.httpBody == nil)
            guard let auth = url.allHTTPHeaderFields?.first(where: { $0.key == "Authorization" }) else {
                throw "missing auth header"
            }
            XCTAssertEqual(auth.value, token)
            
            guard let headerContentType = url.allHTTPHeaderFields?.first(where: { $0.key == "Content-Type" }) else {
                throw "missing content-type header"
            }
            XCTAssertEqual(headerContentType.value, cType)
            
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testGetWithBodyParams() throws {
        let service = ServiceProtocolSpy(path: "posts",
                                         method: .get,
                                         task: .requestParameters(["id": "1"]),
                                         headers: nil,
                                         parametersEncoding: .body)
        do {
            let url = try URLRequest(service: service)
            XCTAssertEqual(url.httpMethod!, HTTPMethod.get.rawValue)
            XCTAssertEqual(url.url?.absoluteString, "http://mango.com/posts")
            XCTAssert(url.httpBody != nil)
            let body = String(data: url.httpBody!, encoding: .utf8)!
            XCTAssertEqual(body, "{\"id\":\"1\"}")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testPostWithBodyParams() throws {
        let service = ServiceProtocolSpy(path: "posts",
                                         method: .post,
                                         task: .requestParameters(["id": "1"]),
                                         headers: nil,
                                         parametersEncoding: .body)
        do {
            let url = try URLRequest(service: service)
            XCTAssertEqual(url.httpMethod!, HTTPMethod.post.rawValue)
            XCTAssertEqual(url.url?.absoluteString, "http://mango.com/posts")
            XCTAssert(url.httpBody != nil)
            let body = String(data: url.httpBody!, encoding: .utf8)!
            XCTAssertEqual(body, "{\"id\":\"1\"}")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testDuplicateHeaders() throws {
        let token = "Bearer 1234"
        let cType = "application/json"
        let auth = Headers.Value(key: .authorization, value: token)
        let contentType = Headers.Value(key: .contentType, value: cType)
        
        var headers: Headers?
        do {
            headers = try Headers(headers: [auth, contentType, auth])
        } catch let error as NetworkError {
            guard case NetworkError.duplicatedHeader = error else {
                throw "unexpected error kind"
            }
        }
        XCTAssert(headers == nil)
    }
}

private extension URLRequestTests {
    struct ServiceProtocolSpy: ServiceProtocol {
        var path: String
        var method: HTTPMethod
        var task: Task
        var headers: Headers?
        var parametersEncoding: ParametersEncoding
        var baseURL: URL {
            return URL(string: "http://mango.com")!
        }
    }
}

extension String: Error { }
