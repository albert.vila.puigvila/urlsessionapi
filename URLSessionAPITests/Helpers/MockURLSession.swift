//
//  MockURLSession.swift
//  URLSessionClientTests
//
//  Created by albert vila  on 04/03/2020.
//  Copyright © 2020 albert vila . All rights reserved.
//

import Foundation

class MockURLSession: URLSessionProtocol {

    enum MockURLSessionResponse {
        case success(json: String)
        case error(Error)
    }
    
    let response: MockURLSessionResponse
    var executingTasks = [MockURLSessionDataTask]()
    var finishedTasks = [MockURLSessionDataTask]()
    
    private let delay: Int
    private let urlResponse: URLResponse

    // MARK: - Life Cycle -
    init(response: MockURLSessionResponse,
         urlResponse: URLResponse = HTTPURLResponse(),
         delayMillis: Int = 10) {
        self.response    = response
        self.urlResponse = urlResponse
        self.delay       = delayMillis
    }
    
    func dataTask(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        let task = MockURLSessionDataTask(urlRequest: request)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            let idx = self.executingTasks.lastIndex(of: task)!
            self.executingTasks.remove(at: idx)
            
            switch self.response {
            case .success(json: let jsonString):
                completionHandler(jsonString.data(using: .utf8), self.urlResponse, nil)
            case .error(let error):
                completionHandler(nil, nil, error)
            }
            self.finishedTasks.append(task)
        }
        executingTasks.append(task)
        return task
    }
}

class MockURLSessionDataTask: URLSessionDataTaskProtocol, Equatable {
    let urlRequest: URLRequest
    var resumeWasCalled = false
    var cancelWasCalled = false

    var state: URLSessionTask.State {
        return .completed
    }
    
    init(urlRequest: URLRequest) {
        self.urlRequest = urlRequest
        self.resumeWasCalled = false
    }

    func resume() {
        assert(!resumeWasCalled)
        resumeWasCalled = true
    }

    func cancel() {
        assert(!cancelWasCalled)
        cancelWasCalled = true
    }

    static func == (lhs: MockURLSessionDataTask, rhs: MockURLSessionDataTask) -> Bool {
        return lhs.urlRequest == rhs.urlRequest
    }
}

extension HTTPURLResponse {
    convenience init(statusCode: Int) {
        self.init(url: URL(string: "htp://mango.com")!, statusCode: statusCode, httpVersion: nil, headerFields: nil)!
    }
}
